@extends('layouts.master')

@section('content')
<section class="banner-area organic-breadcrumb">
    <div class="container">
        <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
            <div class="col-first">
                <h1>My Account</h1>
            </div>
            <div class="col-second">
                <p>So you have your new digital camera and clicking away to glory anything and everything in sight.</p>
            </div>
            <div class="col-third">
                <nav class="d-flex align-items-center flex-wrap justify-content-end">
                    <a href="index.php">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    <a href="#">My Account</a>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- End Banner Area -->

<!-- Start My Account -->
<div class="container">
    <div class="row justify-content-md-center">
        <!-- <div class="col-md-6">
            <div class="login-form">
                <h3 class="billing-title text-center">Login</h3>
                <p class="text-center mt-80 mb-40">Welcome back! Sign in to your account </p>
                <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                    <input type="email" placeholder="Username or Email*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Username or Email*'" required class="common-input mt-20" name="email" value="{{ old('email') }}"  autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <input type="password" placeholder="Password*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Password*'" required class="common-input mt-20" name="password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                    <button class="view-btn color-2 mt-20 w-100"><span>Login</span></button>

                    <div class="mt-20 d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center"><input type="checkbox" class="pixel-checkbox" id="login-1"><label for="login-1">Remember me</label></div>
                        <a href="#">Lost your password?</a>
                    </div>
                </form>
            </div>
        </div> -->

        <div class="col-md-6 ">
            <div class="register-form">
                <h3 class="billing-title text-center">Register</h3>
                <p class="text-center mt-40 mb-30">Create your very own account </p>
                <form method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <input type="text" placeholder="First name*" onfocus="this.placeholder=''" onblur="this.placeholder = 'First name*'" required class="common-input mt-20" name="first_name" value="{{ old('first_name') }}"  autocomplete="off">
                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif

                    <input type="text" placeholder="Last name*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Last name*'" required class="common-input mt-20" name="last_name" value="{{ old('last_name') }}"  autocomplete="off">
                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif


                    <input type="email" placeholder="Email address*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Email address*'" required class="common-input mt-20" name="email" value="{{ old('email') }}"  autocomplete="off">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <input type="password" placeholder="Password*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Password*'" required class="common-input mt-20" name="password"  autocomplete="off">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                    <input type="password" placeholder="Password Confirm*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Password Confirm*'" required class="common-input mt-20" name="password_confirmation"  autocomplete="off">



                    <button class="view-btn color-2 mt-20 w-100"><span>Register</span></button>

                    <div class="mt-20 d-flex align-items-center justify-content-between">
                        <a href="/login">Already have an account?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End My Account -->
<br>
<br>
<br>
<br>
<br>

@endsection
