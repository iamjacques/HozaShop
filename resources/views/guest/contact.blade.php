@extends('layouts.master')

@section('content')

<!-- End Header Area -->	<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center">
				<div class="col-first">
					<h1>Contact</h1>
				</div>
				<div class="col-second">
					<p></p>
				</div>
				<div class="col-third">
					<nav class="d-flex align-items-center justify-content-end">
						<a href="/">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
						<a href="">Contact</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->
	<div class="container">
		<div class="contact-area mt-50">
			<div class="row">
				<div class="col-md-6">
					<div class="map-wraper">
						<div id="map"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="add-review">
						<h3>Send Us Message</h3>
						<form action="#" class="main-form">
							<input type="text" placeholder="Your Full name" onfocus="this.placeholder=''" onblur="this.placeholder = 'Your Full name'" required class="common-input">
							<input type="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" placeholder="Email Address" onfocus="this.placeholder=''" onblur="this.placeholder = 'Email Address'" required class="common-input">
							<input type="text" placeholder="Phone Number" onfocus="this.placeholder=''" onblur="this.placeholder = 'Phone Number'" required class="common-input">
							<textarea placeholder="Messege" onfocus="this.placeholder=''" onblur="this.placeholder = 'Messege'" required class="common-textarea"></textarea>
							<button class="view-btn color-2"><span>Submit Now</span></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
@endsection()