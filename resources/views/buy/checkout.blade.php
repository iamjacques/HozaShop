@extends('layouts.master')

@section('content')
<!-- End Header Area -->	<!-- Start Banner Area -->
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center">
				<div class="col-first">
					<h1>Cart</h1>
				</div>
				<div class="col-second">
					<p>So you have your new digital camera and clicking away to glory anything and everything in sight.</p>
				</div>
				<div class="col-third">
					<nav class="d-flex align-items-center justify-content-end">
						<a href="index.php">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
						<a href="#">Cart</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!-- Start Checkout Area -->
	@if(!Auth::user())
	<div class="container">
		<div class="checkput-login">
			<div class="top-title">
				<p>Returning Customer? <a data-toggle="collapse" href="#checkout-login" aria-expanded="false" aria-controls="checkout-login">Click here to login</a></p>
			</div>
			<div class="collapse" id="checkout-login">
				<div class="checkout-login-collapse d-flex flex-column">
					<p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing & Shipping section.</p>
					<form action="/login" method="POST" class="d-block ajax-login-form">
					{{ csrf_field() }}
						<div class="row">
							<div class="col-lg-4">
								<input type="hidden" name="origin" value="checkout">
								<input type="text" name="email" placeholder="Username or Email*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Username or Email*'" required class="common-input mt-10 email">
								
							</div>
							<div class="col-lg-4">
								<input type="password" name="password" placeholder="Password*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Password*'" required   class="common-input mt-10 password">
							</div>
						</div>
						<div class="d-flex align-items-center flex-wrap">
							<button class="view-btn color-2 mt-20 mr-20 login-btn"><span>Login</span></button>
						</div>
					</form>
					<a href="#" class="mt-10">Lost your password?</a>
				</div>
			</div>
		</div>
	</div>
	@endif
	<!-- End Checkout Area -->
	
	<!-- Start Billing Details Form -->
	<div class="container">
		<form action="#" class="billing-form" name="billing-form" >
			<div class="row">
				<div class="col-lg-8 col-md-6">
					<h3 class="billing-title mt-20 mb-10">Billing Details</h3>
					<div class="row">
						<div class="col-lg-12">
						<input type="checkbox" name="business_cusomter" value="" class="business-shopping no" >
						<label for="business-shopping">Business Shopping?</label>
						</div>

						<div class="col-lg-6">
							<input type="text" placeholder="First name*" onfocus="this.placeholder=''" onblur="this.placeholder = 'First name*'" required class="common-input first_name" value="{{Auth::user() ? Auth::user()->first_name : ''}}" >
						</div>
						<div class="col-lg-6">
							<input type="text" placeholder="Last name*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Last name*'" required class="common-input last_name" value="{{Auth::user() ? Auth::user()->last_name : ''}}">
						</div>
						<div class="business-fields " style="display: none; width: 100%;">
							<div class="col-lg-12">
								<input type="text" placeholder="Company Name*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Company Name*'" required class="common-input company" value="" >
							</div>
							<div class="col-lg-12">
								<input type="text" placeholder="VAT number*" onfocus="this.placeholder=''" onblur="this.placeholder = 'VAT number*'" required class="common-input vat_number" value="" >
							</div>
						</div>
						<div class="col-lg-6">
							<input type="text" placeholder="Phone number" onfocus="this.placeholder=''" onblur="this.placeholder = 'Phone number*'"  class="common-input number" value="" >
						</div>
						<div class="col-lg-6">
							<input type="email" placeholder="Email Address*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Email Address*'" required class="common-input email" value="{{Auth::user() ? Auth::user()->email : ''}}" >
						</div>

						<div class="col-lg-6">
							<input type="text" placeholder="Building Number*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Address line 01*'" required class="common-input building_number" value="{{ (Auth::user() && count(Auth::user()->addresses) > 0) ? Auth::user()->addresses[0]->building_number : ''}}" >
						</div>

						<div class="col-lg-6">
							<input type="text" placeholder="Address line 1*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Address line 01*'" required class="common-input address1" value="{{(Auth::user()  && count(Auth::user()->addresses) > 0) ? Auth::user()->addresses[0]->address1 : ''}}" >
						</div>
						<div class="col-lg-12">
							<input type="text" placeholder="Address line 2" onfocus="this.placeholder=''" onblur="this.placeholder = 'Address line 02*'" required class="common-input address2" value="{{ (Auth::user()  &&  count(Auth::user()->addresses) > 0) ? Auth::user()->addresses[0]->address2 : ''}}">
						</div>
						<div class="col-lg-12">
							<input type="text" placeholder="Postcode/ZIP*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Postcode/ZIP'" required  class="common-input postcode" value="{{(Auth::user() &&  count(Auth::user()->addresses) > 0 ) ? Auth::user()->addresses[0]->postcode : ''}}">
						</div>

						<div class="col-lg-12">
							<input type="text" placeholder="Town/City*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Town/City*'" required class="common-input city" value="{{(Auth::user()  && count(Auth::user()->addresses) > 0) ? Auth::user()->addresses[0]->city : ''}}">
						</div>
						
						<div class="col-lg-12">
							<div class="sorting">
								<select>
									<option value="1">Country*</option>
									<option value="1">Default sorting</option>
									<option value="1">Default sorting</option>
								</select>
							</div>
						</div>

						@if(!Auth::user())
						<div class="col-lg-12">
							<div class="mt-20">
							<input type="checkbox" class="pixel-checkbox can_create_account" ><label for="pixel-checkbox">Create an account?</label></div>
						</div>
						@endif
					</div>
					
					

<!-- 
					<h3 class="billing-title mt-20 mb-10">Billing Details</h3>
					<div class="mt-20"><input type="checkbox" class="pixel-checkbox" id="login-6"><label for="login-6">Ship to a different address?</label></div>
					<textarea placeholder="Order Notes" onfocus="this.placeholder=''" onblur="this.placeholder = 'Order Notes'" required class="common-textarea"></textarea> -->
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="order-wrapper mt-50">
						<h3 class="billing-title mb-10">Your Order</h3>
						<div class="order-list">
							<div class="list-row d-flex justify-content-between">
								<div>Product</div>
								<div>Total</div>
							</div>
							@foreach( $products as  $product)
							<div class="list-row d-flex justify-content-between">
								<div style="width: 80%" >{{$product->product_name}}</div>
								<div>x {{$product->pivot->qty}}</div>
								<div> £{{$product->pivot->qty * $product->price }} </div>
							</div>
							@endforeach

							<div class="list-row d-flex justify-content-between">
								<h6>Subtotal</h6>
								<div>£{{$total}}</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<h6>Shipping</h6>
								<div>£{{$shipping}}</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<h6>Total</h6>
								<div class="total"> £{{$total + $shipping}}</div>
							</div>

							<br> <br>

							<div id="dropin-container"></div>


							<!-- <div class="mt-20 d-flex align-items-start">
							<input type="checkbox" class="pixel-checkbox" id="login-4"><label for="login-4">I’ve read and accept the <a href="#" class="terms-link">terms & conditions*</a></label></div> -->

							
							<a class="view-btn color-2 w-100 mt-20" id="submit-payment"><span> Submit Payment</span></a>

						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<!-- End Billing Details Form -->
	
	


	<div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="payment-modal-dialog modal-dialog" role="document">
                <div class="container relative">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="product-quick-view">
                        <div class="row align-items-center">

                            <div class="col-md-12">
                                <div class="quick-view-content">
                                    <div class="middle">
										<div id="dropin-container"></div>
                                    </div>
                                    <div class="bottom">
                      
                
                                        <div class="d-flex mt-20">
                                            <button id="submit-payment" class="view-btn color-2"><span>Submit Payment</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    
		<br>
	<br>
	<br>
	<br>
	
	
@endsection()