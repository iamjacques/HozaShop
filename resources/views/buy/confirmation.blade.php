@extends('layouts.master')

@section('content')
<!-- End Header Area -->	<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center">
				<div class="col-first">
					<h1>Order Received</h1>
				</div>
				<div class="col-second">
					<p>So you have your new digital camera and clicking away to glory anything and everything in sight.</p>
				</div>
				<div class="col-third">
					<nav class="d-flex align-items-center flex-wrap justify-content-end">
						<a href="index.php">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
						<a href="03-02-woocommerce-checkout.php">Checkout<i class="fa fa-caret-right" aria-hidden="true"></i></a>
						<a href="#">Order Received</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!-- Start Checkout Area -->
	<div class="container">
		<p class="text-center">Thank you. Your order has been received.</p>
		<div class="row mt-50">
			<div class="col-md-4">
				<h3 class="billing-title mt-20 pl-15">Order Info</h3>
				<table class="order-rable">
					<tr>
						<td>Order number</td>
						<td>: 60235</td>
					</tr>
					<tr>
						<td>Date</td>
						<td>: Oct 03, 2017</td>
					</tr>
					<tr>
						<td>Total</td>
						<td>: USD 2210</td>
					</tr>
					<tr>
						<td>Payment method</td>
						<td>: Check payments</td>
					</tr>
				</table>
			</div>
			<div class="col-md-4">
				<h3 class="billing-title mt-20 pl-15">Billing Address</h3>
				<table class="order-rable">
					<tr>
						<td>Street</td>
						<td>: 56/8 panthapath</td>
					</tr>
					<tr>
						<td>City</td>
						<td>: Dhaka</td>
					</tr>
					<tr>
						<td>Country</td>
						<td>: Bangladesh</td>
					</tr>
					<tr>
						<td>Postcode</td>
						<td>: 1205</td>
					</tr>
				</table>
			</div>
			<div class="col-md-4">
				<h3 class="billing-title mt-20 pl-15">Shipping Address</h3>
				<table class="order-rable">
					<tr>
						<td>Street</td>
						<td>: 56/8 panthapath</td>
					</tr>
					<tr>
						<td>City</td>
						<td>: Dhaka</td>
					</tr>
					<tr>
						<td>Country</td>
						<td>: Bangladesh</td>
					</tr>
					<tr>
						<td>Postcode</td>
						<td>: 1205</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<!-- End Checkout Area -->
	<!-- Start Billing Details Form -->
	<div class="container">
		<div class="billing-form">
			<div class="row">
				<div class="col-12">
					<div class="order-wrapper mt-50">
						<h3 class="billing-title mb-10">Your Order</h3>
						<div class="order-list">
							<div class="list-row d-flex justify-content-between">
								<div>Product</div>
								<div>Total</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<div>Pixelstore fresh Blackberry</div>
								<div>x 02</div>
								<div>$720.00</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<div>Pixelstore fresh Blackberry</div>
								<div>x 02</div>
								<div>$720.00</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<div>Pixelstore fresh Blackberry</div>
								<div>x 02</div>
								<div>$720.00</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<h6>Subtotal</h6>
								<div>$2160.00</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<h6>Shipping</h6>
								<div>Flat rate: $50.00</div>
							</div>
							<div class="list-row border-bottom-0 d-flex justify-content-between">
								<h6>Total</h6>
								<div class="total">$2210.00</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Billing Details Form -->
	
	
	<!-- Start Most Search Product Area -->
	<section class="section-half">
		<div class="container">
			<div class="organic-section-title text-center">
				<h3>Most Searched Products</h3>
			</div>
			<div class="row mt-30">
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp1.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Blueberry</a>
							<div class="price"><span class="lnr lnr-tag"></span> $240.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp2.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Cabbage</a>
							<div class="price"><span class="lnr lnr-tag"></span> $189.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp3.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Raspberry</a>
							<div class="price"><span class="lnr lnr-tag"></span> $189.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp4.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Kiwi</a>
							<div class="price"><span class="lnr lnr-tag"></span> $189.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp5.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore Bell Pepper</a>
							<div class="price"><span class="lnr lnr-tag"></span> $120.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp6.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Blackberry</a>
							<div class="price"><span class="lnr lnr-tag"></span> $120.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp7.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Brocoli</a>
							<div class="price"><span class="lnr lnr-tag"></span> $120.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp8.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Carrot</a>
							<div class="price"><span class="lnr lnr-tag"></span> $120.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp9.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Strawberry</a>
							<div class="price"><span class="lnr lnr-tag"></span> $240.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp10.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Prixma MG2 Light Inkjet</a>
							<div class="price"><span class="lnr lnr-tag"></span> $240.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp11.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Cherry</a>
							<div class="price"><span class="lnr lnr-tag"></span> $240.00 <del>$340.00</del></div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="img/organic-food/mp12.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Beans</a>
							<div class="price"><span class="lnr lnr-tag"></span> $240.00 <del>$340.00</del></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Most Search Product Area -->
@endsection()