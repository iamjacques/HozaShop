@extends('layouts.admin')

@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Add New  Product </h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
            <a href="/admin/products" class="btn btn-sm btn-outline-secondary"> Back </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            @if (session('success'))
                <div class="alert alert-success">
                    New product added!
                </div>
            @endif
        </div>

        <div class="col-6">
            <form  method="POST" action="/admin/new/product">
            {{ csrf_field() }}
                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label for="formGroupExampleInput"> Product Name </label>
                            <input type="text" class="form-control" name="product_name"  value="{{ old('product_name') }}">
                            @if ($errors->has('product_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('product_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2"> Product Code</label>
                            <input type="text" class="form-control" name="product_code" value="{{ old('product_code') }}">
                            @if ($errors->has('product_code'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('product_code') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2"> Price </label>
                            <input type="text" class="form-control" name="price" value="{{ old('price') }}">
                            @if ($errors->has('price'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2"> Stock</label>
                            <input type="text" class="form-control" name="stock"  value="{{ old('stock') }}">
                            @if ($errors->has('stock'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('stock') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2"> Weight </label>
                            <input type="text" class="form-control" name="weight" value="{{ old('weight') }}">
                            @if ($errors->has('weight'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('weight') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Description</label>
                            <textarea class="form-control"  name="product_description" rows="7" >{{ old('product_description') }} </textarea>
                            @if ($errors->has('product_description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('product_description') }}</strong>
                                </span>
                            @endif

                            
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form> 
        </div>
        <div class="col-6">
        <label for="formGroupExampleInput2"> Product images </label>
            <form action="/file-upload" class="dropzone">
            
                <div class="fallback">
                    <input name="file" type="file" multiple />
                </div>
            </form>
        </div>
    </div>
</main>

@endsection
