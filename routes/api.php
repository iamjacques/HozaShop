<?php


use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function(){
    Route::post('/cart', 'CartCtrl@getItem');
    Route::post('/cart/add', 'CartCtrl@addItem');
    Route::post('/cart/remove', 'CartCtrl@removeItem');
    Route::post('/register', 'RegisterCtrl@register');


    Route::post('/payment/token', 'PaymentCtrl@token');
    Route::post('/payment', 'PaymentCtrl@payment');
});

Route::post('/login', function (Request $request) {
    
});
