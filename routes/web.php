<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', 'Guest\GuestCtrl@test');


Route::get('/', 'Guest\GuestCtrl@index');
Route::get('/products', 'Guest\GuestCtrl@products');
Route::get('/product/{id}', 'Guest\GuestCtrl@product');
Route::get('/about', 'Guest\GuestCtrl@about');
Route::get('/contact', 'Guest\GuestCtrl@contact');
Route::post('/auth', 'Guest\GuestCtrl@auth');

//Buy 
Route::get('/cart/{id}', 'Checkout\CheckoutCtrl@cart');
Route::get('/checkout/{id}', 'Checkout\CheckoutCtrl@checkout');
Route::get('/checkout/token', 'Checkout\CheckoutCtrl@token');
Route::get('/confirmation', 'Checkout\CheckoutCtrl@confirmation');

Auth::routes();

//Admin 
Route::group(['namespace' => 'Admin'], function()
{
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/dashboard', 'AdminCtrl@index');
        Route::get('/products', 'AdminCtrl@products');
        Route::get('/new/product', 'AdminCtrl@addProduct');
        Route::get('/update/product/{id}', 'AdminCtrl@updateProduct');
        Route::post('/new/product', 'AdminCtrl@postProduct');
        Route::post('/update/product/{id}', 'AdminCtrl@postUpdateProduct');
    });
});





