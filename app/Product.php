<?php

namespace App;

use \App\Order;
use \App\Cart;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = [
        'product_name', 
        'product_code', 
        'product_description', 
        'stock',
        'price',
        'weight',
        'slug',
        'taxable'
    ];


    public function carts(){
        return $this->belongsToMany(Cart::class, 'cart_product')->withPivot('qty');
    }

    public function order(){
        return $this->belongsToMany(Order::class, 'orders_products')->withPivot('qty');
    }
}
