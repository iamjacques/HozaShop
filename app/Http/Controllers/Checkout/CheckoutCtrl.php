<?php

namespace App\Http\Controllers\Checkout;
use \App\User;
use \App\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CheckoutCtrl extends Controller
{
    
    public function cart($id){

        $cart = Cart::where('cart_id', $id)->first();
        if(!$cart){
            return redirect('/');  
        }
      
        $products =  $cart->products;
        $total = 0;
        $cart_id = $cart->cart_id;

        foreach($products as $product){
            $price =  $product->price * $product->pivot->qty;
            $total = $total +  $price;
        }  

        return view("buy.cart", compact('products', 'cart_id', 'total' ));
    }
    
    public function checkout($id){
       
        $cart = Cart::where('cart_id', $id)->first();
        if(!$cart ){
            return redirect('/');  
        }
      
        $products =  $cart->products;
        $total = 0;
        
        foreach($products as $product){
            $price =  $product->price * $product->pivot->qty;
            $total = $total +  $price;
        }
        $shipping = 0;
        $checkout = true;

           return view("buy.checkout", compact('shipping', 'products', 'checkout', 'total'));
     }

     public function confirmation(){
        return view("buy.confirmation");
    }
}
