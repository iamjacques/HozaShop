<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminCtrl extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(){
        return view('admin.dashboard');
    }


    public function products(){
        $products = Product::all();
        return view('admin.products')->with(compact('products'));
    }

    public function addProduct(){
        return view('admin.product');
    }

    public function postProduct(Request $request){
        $validator = Validator::make($request->all(), [
            'product_name' => 'required|max:255',
            'product_code' => 'required|unique:products',
            'product_description' => 'required',
            'stock' => 'required|numeric',
            'price' => 'required|numeric',
            'weight' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/new/product')
                        ->withErrors($validator)
                        ->withInput();
        }

        $product =   Product::create([
            'product_name' => $request->product_name,
            'product_code' => $request->product_code,
            'product_description' => $request->product_description,
            'stock' =>  $request->stock,
            'price' =>  $request->price,
            'weight' =>  $request->weight,
            'slug' => 'dddd',
            'taxable' => true,
        ]);

      if($product){
        return  back()->with('success', true);
      }
    }


    public function updateProduct($id){

       $prodct =   Product::findOrFail($id);
       return view('admin.product')->with(compact($prodct));
    }
}
