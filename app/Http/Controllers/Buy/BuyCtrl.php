<?php

namespace App\Http\Controllers\Buy;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BuyCtrl extends Controller
{
    public function cart(){
        return view("buy.cart");
    }

    public function checkout(){

        return view("buy.checkout")->with('checkout', true);
    }

    public function confirmation(){
        return view("buy.confirmation");
    }
}
