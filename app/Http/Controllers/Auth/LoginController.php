<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';
    protected $loginPath = '/login';
    protected $redirectPath = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->loginPath = str_replace(url('/'), '', url()->previous());
    }

    public function login(Request $request){

        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials  = array('email' => $request->email, 'password' => $request->password, 'active' => 1);
  
        if (Auth::attempt($credentials, $request->has('remember'))){

            if($request->origin ===  'checkout'){
                $this->redirectPath =  $this->loginPath ;
            }
    
            if($request->origin ===  'login'){
                switch (Auth::user()->user_type) {
                    case 'admin':
                            $this->redirectPath = '/admin/dashboard';
                        break;
                    case 'business':
                            $this->redirectPath = '/business/dashboard';
                        break;
                    case 'customer':
                        $this->redirectPath = '/dashboard';
                        break;
                }
            }
            
            return redirect()->intended($this->redirectPath);
        } 

        return redirect($this->loginPath)
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => 'Incorrect email address or password',
            ]);
    }

}
